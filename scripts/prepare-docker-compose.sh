#!/bin/sh
cat << EOF
version: "3"

networks:
  proxy:
    external: true
  internal:
    external: false

services:
  $CI_PROJECT_NAME:
    image: $CI_REGISTRY_IMAGE:$CI_COMMIT_SHA
    container_name: $CI_PROJECT_NAME-$CI_PROJECT_ID-$CI_COMMIT_REF_SLUG
    labels:
      - traefik.backend=$CI_PROJECT_NAME_$CI_COMMIT_REF_SLUG
      - traefik.frontend.rule=Host:$APP_DEPLOY_URL
      - traefik.docker.network=proxy
      - traefik.port=$APP_PORT
    # environment:
    # add your other vars for PROD here!   
    # - KEY=$VAR
    networks:
      - internal
      - proxy
    depends_on:
      - postgres

  postgres:
    image: postgres:9.6
    container_name: postgres-$CI_PROJECT_ID-$CI_COMMIT_REF_SLUG
    environment:
      - POSTGRES_DB=$POSTGRES_DB
      - POSTGRES_USER=$POSTGRES_USER
      - POSTGRES_PASSWORD=$POSTGRES_PASSWORD
    networks:
      - internal
    labels:
      - traefik.enable=false            
EOF
